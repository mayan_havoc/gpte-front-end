import { useEffect, useState } from "react";
import type { AssetExtended } from "@meshsdk/core";
import { useWallet } from "@meshsdk/react";
import {
    Box, Heading, Text
} from "@chakra-ui/react"
import { treasury } from "../../cardano/plutus/treasuryContract";

const policyId = treasury.accessTokenPolicyId

export default function ContributorTokens() {
    const { connected, wallet } = useWallet();
    const [loading, setLoading] = useState(false)
    const [connectedContributorTokens, setConnectedContributorTokens] = useState<AssetExtended[] | undefined>(undefined)

    useEffect(() => {
        const fetchContributorToken = async () => {
            const _token = await wallet.getPolicyIdAssets(policyId)
            if (_token.length > 0) {
                setConnectedContributorTokens(_token)
            }
            setLoading(false)
        }

        if (connected) {
            setLoading(true)
            fetchContributorToken()
        }
    }, [connected])

    return (
        <Box mb='5' p='5' border='1px' borderRadius='lg'>
            <Heading size='lg'>Your Contributor Tokens: </Heading>
            {loading ? (<Text>Loading...</Text>) : (
                <>
                    {connectedContributorTokens?.map(token => (
                        <Text key={token.assetName} p='2'>{token.assetName}</Text>
                    ))}
                </>
            )}
        </Box>
    );
}