import { useQuery, gql } from "@apollo/client";
import { Box, Heading, Text, Center, Spinner, Grid } from "@chakra-ui/react";

import DistributionCountWidget from "../tracking/DistributionCountWidget";
import { treasury } from "../../cardano/plutus/treasuryContract";
import { GraphQLToken, GraphQLUTxO } from "../../types";


const TREASURY_UTXO_QUERY = gql`
  query GetTreasuryUTxOs($contractAddress: String!) {
    utxos(where: { address: { _eq: $contractAddress } }) {
      value
      tokens {
        asset {
          policyId
          assetName
        }
        quantity
      }
      datum {
        value
      }
    }
  }
`;

export default function TreasurySummary() {
  const treasuryAddress = treasury.address;
  let gimbalDivisor = 1;
  if(treasury.network == "1") {
    gimbalDivisor = 1000000
  }

  let _lovelaceAtContract = 0
  let _tokenProjectAtContract = 0
  let _approvedProjects = new Set<string>()

  const { data, loading, error } = useQuery(TREASURY_UTXO_QUERY, {
    variables: {
      contractAddress: treasuryAddress,
    },
  })

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    )
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  if (data) {
    data.utxos.map((utxoFromQuery: GraphQLUTxO) => {
 
      _lovelaceAtContract += parseInt(utxoFromQuery.value)

      utxoFromQuery.tokens.map((token: GraphQLToken) =>  {
        if ((token.asset.policyId + token.asset.assetName) === treasury.projectTokenAssetId) {
          _tokenProjectAtContract += parseInt(token.quantity)
        }
      })

      utxoFromQuery.datum?.value.fields[0].list.map((project: any)=> {
        if ( !_approvedProjects.has(project.bytes) ) {
          _approvedProjects.add(project.bytes)
        }
      })

    })
  }

  return (
    <Box
      w="90%"
      mx="auto"
      mt="5"
      p="5"
      bg="gray.700"
      borderRadius="lg"
      boxShadow="2xl"
    >
      <Heading color="white">Treasury Instance</Heading>
      { _lovelaceAtContract > 0 ? (
        <>
          <Heading size="sm" mt="3" color="white">
            Contract Address: {treasuryAddress}
          </Heading>
          <Grid templateColumns="repeat(2, 1fr)" gap="5" mt="5">
            <Box p="5" bg="gray.900" textAlign="center">
              <Heading size="3xl">
                { _lovelaceAtContract / 1000000}
              </Heading>
              <Text>ada</Text>
            </Box>
            <Box p="5" bg="gray.900" textAlign="center">
              <Heading size="3xl">
                { _tokenProjectAtContract / gimbalDivisor}
              </Heading>
              <Text>gimbals</Text>
            </Box>
            <Box p="5" bg="gray.900" textAlign="center">
              <Heading size="3xl">{ _approvedProjects.size }</Heading>
              <Text>Live Projects</Text>
            </Box>
            <DistributionCountWidget />
          </Grid>
        </>
      ) : (
        "Not initialized"
      )}
    </Box>
  );
}
