import Link from "next/link";
import { ExternalLinkIcon } from "@chakra-ui/icons";
import { Box, Heading, Text, Center, Grid, Flex, Link as ChakraLink, Divider, Spacer } from "@chakra-ui/react";

import { filterTransactionsById, getContributorTokenNames, getCommitmentStatus } from "../../components/tracking/CommitmentStatus";
import { approvalProcess } from "../../project-lib/approvalProcess";
import { Project, rawTxEscrow } from "../../types";


type Props = {
  project: Project;
  txToEscrow: rawTxEscrow[];
  txFromEscrow: rawTxEscrow[];
};

const ProjectCardRounded: React.FC<Props> = ({ project: p, txToEscrow: txT, txFromEscrow: txF  }) => {

  const txToEscrow = filterTransactionsById(txT, p.id);
  const txFromEscrow = filterTransactionsById(txF, p.id);
  let status = getCommitmentStatus(getContributorTokenNames(txToEscrow, "to"),getContributorTokenNames(txFromEscrow, "from"), p.multipleCommitments)
  
  let projectColor = "gray.700";
  let projectColorLight = "gray.600"
  let projectColorHover = "gray.100"

  if (p.devCategory === "Data") {
    projectColor = "blue.900";
    projectColorLight = "blue.800"
    projectColorHover = "blue.100"
  } else if (p.devCategory === "Documentation") {
    projectColor = "teal.900";
    projectColorLight = "teal.800"
    projectColorHover = "teal.100"
  } else if (p.devCategory === "Education") {
    projectColor = "teal.900";
    projectColorLight = "teal.800"
    projectColorHover = "teal.100"
  } else if (p.devCategory === "Front End") {
    projectColor = "pink.900";
    projectColorLight = "pink.800"
    projectColorHover = "pink.100"
  } else if (p.devCategory === "Plutus") {
    projectColor = "purple.900";
    projectColorLight = "purple.800"
    projectColorHover = "purple.100"
  }


  return (
    <Flex key={p.id} flexDir="column" px="2" bg={projectColor} borderRadius="2xl">

      <Center mt="1">
        <Text pb="1" color="gray.200" fontSize="md">#{p.id}</Text>
      </Center>

      <Divider color="gray.200"/>

      <Center my="1" py="1">
        <Heading color="gray.200" size="lg" textAlign="center">
          {p.title}
        </Heading>
      </Center>
      
      <Divider color="gray.200"/>
        
      <Box mx="12" my="4" py="3" bg={projectColorLight} borderRadius="2xl">
        <Grid templateColumns="repeat(2, 1fr)">
          <Center flexDir="column">
            <Heading color="gray.200" size="lg">
              {p.gimbals}
            </Heading>
            <Text fontSize="sm" color="gray.200" pt="1">Gimbals</Text>
          </Center>
          <Center flexDir="column">
            <Heading color="gray.200" size="lg">
              {p.lovelace / 1000000}
            </Heading>
            <Text fontSize="sm" color="gray.200" pt="1">Ada</Text>
          </Center>
        </Grid>
      </Box>

      <Text fontSize="sm" mx="2" p="1">Category: {p.devCategory}</Text>
      <Text fontSize="sm" mx="2" p="1">Posted: {p.datePosted}</Text>
      <Text fontSize="sm" mx="2" p="1">{p.approvalProcess ? `Approval Process: ${approvalProcess[p.approvalProcess - 1].name}` : ""}</Text>
      <Text fontSize="sm" mx="2" p="1">Multiple commitments: {p.multipleCommitments ? "Yes" : "No"}</Text>

      <Spacer />

      { status[0] == "Open for Commitment" ? (
        <Center mx="1" mt="3" mb="2" py="2" bg="green.600" borderRadius="lg">
          <Text fontSize="sm" as='b' color='gray.200'>Open for Commitment</Text>
        </Center>
      ) : (
        <Center mx="1" mt="3" mb="2" py="2" bg="red.600" borderRadius="lg">
          <Text fontSize="sm" as='b' color="gray.200">Closed</Text>
        </Center>
      )}

      {p.status === "Coming Soon" && (
        <Center borderRadius="lg" mx="1" py="2" bg="gray.200" color={projectColor}>
          Coming Soon
        </Center>
      )}
      {p.status === "Complete" && (
        <Center borderRadius="lg" mx="1" py="2" bg="gray.200" color={projectColor}>
          Complete!
        </Center>
      )}
      {p.status === "Open" && (
        <Link href={`/projects/${p.id}`}>
          <Center borderRadius="lg" mx="1" py="2" bg="gray.200" color={projectColor} _hover={{ bg:projectColorHover, cursor: "pointer" }}>
            View Details {p.id}
          </Center>
        </Link>
      )}

      <Center my="1">
        <ChakraLink href={p.repositoryLink} isExternal color="gray.300" fontSize="sm">
          Project Repo <ExternalLinkIcon mx='2px' />
        </ChakraLink>
      </Center>

    </Flex>
  );
};

export default ProjectCardRounded;
