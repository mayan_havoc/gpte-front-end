import Link from "next/link";
import { Project } from "../../types";
import { rawTxEscrow } from "../../types";
import { ExternalLinkIcon } from "@chakra-ui/icons";


import {
  Box,
  Badge,
  Image,
  Heading,
  Center,
  Link as ChakraLink,
  Button,
} from "@chakra-ui/react";


import { filterTransactionsById, getContributorTokenNames, getCommitmentStatus } from "../tracking/CommitmentStatus";


type Props = {
  project: Project;
  txToEscrow: rawTxEscrow[];
  txFromEscrow: rawTxEscrow[];
};

const ProjectCard: React.FC<Props> = ({ project: p, txToEscrow: txT, txFromEscrow: txF  }) => {
  const txToEscrow = filterTransactionsById(txT, p.id);
  const txFromEscrow = filterTransactionsById(txF, p.id);
  let status = getCommitmentStatus(getContributorTokenNames(txToEscrow, "to"),getContributorTokenNames(txFromEscrow, "from"), p.multipleCommitments)
  
  let image = {
    imageUrl: "https://cdn-icons-png.flaticon.com/512/1055/1055666.png",
    imageAlt: "Front end design category"
  }
  
  if (p.devCategory === "Data") {
    image = {
      imageUrl: 'https://cdn-icons-png.flaticon.com/512/993/993688.png',
      imageAlt: 'Data development category', 
    }
  }

  if (p.devCategory === "Documentation") {
    image = {
      imageUrl: 'https://cdn-icons-png.flaticon.com/512/2015/2015058.png',
      imageAlt: 'Development documentation category', 
    }
  }
  
  if (p.devCategory === "Education") {
    image = {
      imageUrl: 'https://cdn-icons-png.flaticon.com/512/3976/3976625.png',
      imageAlt: 'Development education category', 
    }
  }

  if (p.devCategory === "Plutus") {
    image = {
      imageUrl: 'https://cdn-icons-png.flaticon.com/512/1163/1163412.png',
      imageAlt: 'Plutus development category', 
    }
  }


  let projectColor = "gray.50";

  if (p.devCategory === "Data") {
    projectColor = "red.100";
  }

  if (p.devCategory === "Documentation") {
    projectColor = "teal.900";
  }

  if (p.devCategory === "Education") {
    projectColor = "teal.900";
  }

  if (p.devCategory === "Front End") {
    projectColor = "pink.900";
  }

  if (p.devCategory === "Plutus") {
    projectColor = "purple.900";
  }

  return (
    <Box maxW='sm' borderWidth='1px' borderRadius='lg' overflow='hidden' bgColor='whiteAlpha.300'>
      <Image src={image.imageUrl} alt={image.imageAlt} padding='25px' />
      <Box p='6'>
        <Box display='flex' alignItems='baseline'>
           {
             status[0] == "Open" ?
           <Badge borderRadius='full' px='2' colorScheme='green'>
            {status[0]}
           </Badge>
            :
            <Badge borderRadius='full' px='2' colorScheme='red'>
            {status[0]}
           </Badge>
          }
           <Box
            color='white'
            fontWeight='semibold'
            letterSpacing='wide'
            fontSize='xs'
            textTransform='uppercase'
            ml='2'
           >
            Posted: {p.datePosted} Category: {p.devCategory}
           </Box>
        </Box>
        <Box
          mt='1'
          fontWeight='semibold'
          fontSize='2xl'
          as='h4'
          lineHeight='tight'
          noOfLines={2}
        >
          {p.title}
        </Box>
        <Box>
          <Heading as='h4' fontSize='x-large' color='gold'>Rewards</Heading>
          <Box
            fontWeight='semibold'
            letterSpacing='wide'
           >
            Gimbals: {p.gimbals} &bull; Ada: {p.lovelace/1000000}
           </Box>
        </Box>
        <Box
          display='flex'
          justifyContent='start'
          width='100%'
        >
        {p.status === "Open" && (
          <Button colorScheme='teal' my='2' fontSize='sm' _hover={{ bg: "green.100", cursor: "pointer", color: 'black'}}>
            <Link href={`/projects/${p.id}`}>
              <Center
                py="3"
              >
                Details
              </Center>
            </Link>
          </Button>
          )}
          <Button m='2' colorScheme='orange' _hover={{ bg: 'white', cursor: "pointer" }}>    
            <ChakraLink href={p.repositoryLink} isExternal fontSize='sm' color='black' _hover={{ color: 'orange' }}>
              Project repo <ExternalLinkIcon mx='2px' />
            </ChakraLink>
          </Button>
        </Box>
      </Box>
    </Box>
  );
};

export default ProjectCard;
