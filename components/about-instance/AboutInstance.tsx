import { Box, Text, Link } from "@chakra-ui/react";

export default function AboutInstance() {
  return (
    <Box width="70%" mx="auto" p="5">
      <Text fontSize="2xl" pb="2">
        This is a Testnet Instance of GPTE
      </Text>
      <Text fontSize="lg" pb="2">
        It is currently a minimal implementation, and we are using this
        instance of GPTE to organize the work of building it. All of the{" "}
        <Link href="/projects">Projects</Link> listed here are necessary steps
        toward making the dapp more accessible to developers and user-friendly
        for anyone.
      </Text>
      <Text fontSize="lg" pb="2">
        Soon, additional instances of GPTE will be released for specific
        projects. To learn how to build an instance of GPTE, you can enroll in{" "}
        <Link href="https://gimbalabs.instructure.com/enroll/3CFNFB">
          Plutus Project-Based Learning
        </Link>{" "}
        or join{" "}
        <Link href="https://gimbalabs.com/calendar">
          weekly Live Coding sessions at Gimbalabs
        </Link>
        . If you&apos;d like help deploying an instances of GPTE, drop by and
        say hi on{" "}
        <Link href="https://discord.com/invite/Va7DXqSSn8">
          Gimbalabs Discord
        </Link>
        .
      </Text>
    </Box>
  );
}
