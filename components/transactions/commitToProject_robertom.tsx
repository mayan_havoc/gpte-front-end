import { useEffect, useState } from "react";
import { useQuery, gql } from "@apollo/client";
import {
  Box,
  Heading,
  Text,
  Button,
  Badge,
  Image,
  Center,
  Spinner,
  Accordion,
  AccordionItem,
  AccordionButton,
  AccordionPanel,
  AccordionIcon,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure,
  Alert,
  AlertIcon,
  useClipboard,
  Flex,
  useToast,
  HStack
} from "@chakra-ui/react";

// Mesh and a custom useWallet hook.
import { resolveDataHash, resolvePaymentKeyHash, Transaction } from "@meshsdk/core";
import type { Action, Asset, AssetExtended, Data, UTxO } from "@meshsdk/core";
import { useWallet } from "@meshsdk/react";

// Additional Project Imports:
import {
  Project,
  ProjectDatum,
  ProjectTxMetadata,
  GraphQLUTxO,
} from "../../types";
import {
  treasury,
  treasuryPlutusScript,
  treasuryDatum,
  treasuryReferenceUTxO,
} from "../../cardano/plutus/treasuryContract";
import { escrow } from "../../cardano/plutus/escrowContract";
import { approvalProcess } from "../../project-lib/approvalProcess";

// Query the Treasury Address
const TREASURY_QUERY = gql`
  query GetTreasuryUTxOs($contractAddress: String!) {
    utxos(where: { address: { _eq: $contractAddress } }) {
      txHash
      index
      address
      value
      tokens {
        asset {
          policyId
          assetName
        }
        quantity
      }
      datum {
        bytes
      }
      script {
        type
        hash
      }
    }
  }
`;

// So we can pass Project props to the commitment button component.
type Props = {
  projectData: Project;
};

const CommitToProject: React.FC<Props> = ({ projectData }) => {
  // Contributor connects a wallet:
  const { connected, wallet } = useWallet();
  const [connectedPkh, setConnectedPkh] = useState<string>("");
  const [contribAddress, setContribAddress] = useState<string>("");
  const [connectedContributorToken, setConnectedContributorToken] = useState<
    AssetExtended | undefined
  >(undefined);
  const [connectedUtxos, setConnectedUtxos] = useState<UTxO[]>([]);

  // Utils
  const [txLoading, setTxLoading] = useState(false);
  const [successfulTxHash, setSuccessfulTxHash] = useState<string | null>(null);

  // Project will expire in 1 month
  const [expirationTime, setExpirationTime] = useState(0); // POSIX time, in milliseconds
  const [expirationDate, setExpirationDate] = useState(""); // user-friendly date string

  // Specific to each Project:
  const [currentProjectDatum, setCurrentProjectDatum] =
    useState<ProjectDatum | null>(null);
  const [currentTreasuryRedeemer, setCurrentTreasuryRedeemer] =
    useState<Partial<Action> | null>(null);
  const [projectDatumHash, setProjectDatumHash] = useState<string>("");
  const [projectTxMetadata, setProjectTxMetadata] =
    useState<ProjectTxMetadata | null>(null);
  const [constructedProjectDatum, setConstructedProjectDatum] = useState<
    Data | undefined
  >(undefined);

  // And finally Transaction Building:
  const [utxoBackToTreasury, setUtxoBackToTreasury] = useState<
    Partial<UTxO> | undefined
  >(undefined);
  const [utxoToProjectEscrow, setUtxoToProjectEscrow] = useState<
    Partial<UTxO> | undefined
  >(undefined);

  const [loadContrib, setLoadContrib] = useState(true);

  const metadataKeyInt = parseInt(treasury.metadataKey);
  const treasuryDatumHash = resolveDataHash(treasuryDatum);

  // Helpful data views:
  const [showDevStuff, setShowDevStuff] = useState(false);

  // For Chakra Modal:
  const { isOpen: isConfirmationOpen , onOpen: onConfirmationOpen, onClose: onConfirmationClose } = useDisclosure()
  const { isOpen: isSuccessOpen , onOpen: onSuccessOpen, onClose: onSuccessClose } = useDisclosure()
  const { onCopy, value, setValue, hasCopied } = useClipboard("");
  const [copyValue, setCopyValue] = useState(false)
  const toast = useToast()

  let gimbalDivisor = 1;
  if (treasury.network == "1") {
    gimbalDivisor = 1000000;
  }

  const gimbalsWithZeros = projectData.gimbals * gimbalDivisor;

  // Copy value to clipboard if copyValue just set to true
  // Enables to set the value just before copying,
  // is there a better way to do this?
  useEffect(() => {
    if (copyValue) {
      onCopy()
      setCopyValue(false)
    }
  }, [copyValue])

  // Set current time and expirationTime
  useEffect(() => {
    const _expTime = new Date(Date.now());
    const currentMonth = _expTime.getMonth();
    _expTime.setMonth(currentMonth + 1);
    const result = _expTime.valueOf();
    const resultString = _expTime.toLocaleDateString();
    setExpirationTime(result);
    setExpirationDate(resultString);
  }, []);

  useEffect(() => {
    if (!utxoBackToTreasury) setLoadContrib(false);
  });

  // Check the connected wallet for Contributor tokens
  // If there are many, return the first one in the list returned by getPolicyIdAssets
  // To do: Implement Contributor token selection
  useEffect(() => {
    const fetchContributorToken = async () => {
      const _token = await wallet.getPolicyIdAssets(
        treasury.accessTokenPolicyId
      );
      if (_token.length > 0) {
        setConnectedContributorToken(_token[0]);
      }
    };

    const fetchContributorUtxo = async () => {
      const _utxos = await wallet.getUtxos();
      if (_utxos.length > 0) {
        setConnectedUtxos(_utxos);
      }
    };

    if (connected) {
      fetchContributorToken();
      fetchContributorUtxo();
      setLoadContrib(true);
    }
  }, [connected, loadContrib]);

  // Get the pkh of Address holding Contributor Token:
  useEffect(() => {
    if (connectedUtxos && connectedContributorToken) {
      const utxoWithContribToken = connectedUtxos.filter(
        (utxo) =>
          utxo.output.amount.filter(
            (a) => a.unit == connectedContributorToken.unit
          ).length > 0
      );
      console.log("Contrib utxos", utxoWithContribToken);
      const _contribAddress = utxoWithContribToken[0].output.address;
      setContribAddress(_contribAddress)
      const result = resolvePaymentKeyHash(_contribAddress);
      setConnectedPkh(result);
    }
  }, [connectedUtxos]);

  // Create the Project Datum and Project Metadata
  // Trigger useEffect to update when wallet is connected, or if expirationTime changes
  useEffect(() => {
    const _result: ProjectDatum = {
      contributorPkh: connectedPkh,
      lovelace: projectData.lovelace,
      gimbals: gimbalsWithZeros,
      expirationTime: expirationTime,
      projectHash: projectData.projectHash,
    };
    const _metadata: ProjectTxMetadata = {
      id: projectData.id,
      hash: projectData.projectHash,
      expTime: expirationTime,
      txType: "Commitment",
      contributor: connectedPkh,
    };
    setCurrentProjectDatum(_result);
    setProjectTxMetadata(_metadata);
  }, [connected, expirationTime, connectedPkh]);

  // constructedProjectDatum is formatted for serialized transaction, using Mesh Data type
  useEffect(() => {
    if (currentProjectDatum) {
      const _datumConstructor: Data = {
        alternative: 0,
        fields: [
          currentProjectDatum.contributorPkh,
          currentProjectDatum.lovelace,
          currentProjectDatum.gimbals,
          currentProjectDatum.expirationTime,
          currentProjectDatum.projectHash,
        ],
      };

      console.log("Build Escrow Datum:", _datumConstructor);
      setConstructedProjectDatum(_datumConstructor);
      const result = resolveDataHash(_datumConstructor);
      setProjectDatumHash(result);
    }
  }, [currentProjectDatum]);

  // Set the data treasuryRedeemer to match constructedProjectDatum,
  // because in GPTE Contracts, the Treasury Redeemer and Project Datum consist of the same parameters.
  useEffect(() => {
    if (constructedProjectDatum) {
      const _treasuryRedeemer: Partial<Action> = {
        data: {
          alternative: 0,
          fields: [constructedProjectDatum],
        },
      };
      setCurrentTreasuryRedeemer(_treasuryRedeemer);
    }
  }, [constructedProjectDatum]);

  // Create the UTxOs to be included in .sendValue() to Treasury and Escrow contracts
  // UTxO type looks like this:
  // -----------------------------------------------------------
  // export declare type UTxO = {
  //     input: {
  //         outputIndex: number;
  //         txHash: string;
  //     };
  //     output: {
  //         address: string;
  //         amount: Asset[];
  //         dataHash?: string;
  //         plutusData?: string;
  //         scriptRef?: string;
  //     };
  // };
  // -----------------------------------------------------------
  // Here, we use the output: {} in a Partial<UTxO>, and do not include input: {}. This approach is compatible with .sendValue(),
  // and allows us to specify the number of Lovelace in each output.
  //
  // Note also that Asset[] is constructed first for each UTxO.
  // If you are unfamiliar with TypeScript, this is a helpful example to study.
  useEffect(() => {
    if (connectedContributorToken && _contract_utxos.length > 0) {
      const assetsAtTreasury: Asset[] = _contract_utxos[0].output.amount;

      // Calculate the number of Lovelace that will be sent back to Treasury
      const lovelaceAtTreasury = assetsAtTreasury.filter(
        (asset) => asset.unit === "lovelace"
      );
      const numberLovelaceAtTreasury = parseInt(lovelaceAtTreasury[0].quantity);
      const numberLovelaceBackToTreasury =
        numberLovelaceAtTreasury - projectData.lovelace;

      // Calculate the number of tgimbals that will be sent back to Treasury
      const gimbalsAtTreasury = assetsAtTreasury.filter(
        (asset) => asset.unit === treasury.projectTokenAssetId
      );
      const gimbalsInCommitment = gimbalsWithZeros;
      const numberGimbalsAtTreasury = parseInt(gimbalsAtTreasury[0].quantity);
      const numberGimbalsBackToTreasury =
        numberGimbalsAtTreasury - gimbalsInCommitment;

      // Create Asset[] for each output UTxO
      const _assetsBackToTreasury: Asset[] = [
        {
          unit: "lovelace",
          quantity: numberLovelaceBackToTreasury.toString(),
        },
        {
          unit: treasury.projectTokenAssetId,
          quantity: numberGimbalsBackToTreasury.toString(),
        },
      ];

      const _assetsToProjectEscrow: Asset[] = [
        {
          unit: "lovelace",
          quantity: projectData.lovelace.toString(),
        },
        {
          unit: treasury.projectTokenAssetId,
          quantity: gimbalsInCommitment.toString(),
        },
        {
          unit: connectedContributorToken.unit,
          quantity: "1",
        },
      ];

      // Create the UTxOs
      const _utxoTreasury: Partial<UTxO> = {
        output: {
          address: treasury.address,
          amount: _assetsBackToTreasury,
          // dataHash: treasuryDatumHash,
          // plutusData: resolveDataHash(treasuryDatum)
        },
      };

      const _utxoProjectEscrow: Partial<UTxO> = {
        output: {
          address: escrow.address,
          amount: _assetsToProjectEscrow,
          // dataHash: projectDatumHash
          // plutusData: resolveDataHash(constructedProjectDatum)
        },
      };

      console.log("Datum Hashes");
      console.log(treasuryDatumHash);
      console.log(projectDatumHash);
      setUtxoBackToTreasury(_utxoTreasury);
      setUtxoToProjectEscrow(_utxoProjectEscrow);
    }
  }, [connected, connectedContributorToken]);

  // Use the GraphQL query defined at the top of this file
  // To look on-chain at the Treasury Contract Address.
  // Get all the UTxO's from that Address
  // One goal may be to manage a Treasury by ensuring that it always holds exactly one UTxO...
  // ...but we cannot assume that this will always be the case.
  // Projects will be posted for handling cases where:
  // (a) Additional UTxOs are accidentally sent to Treasury Contract
  // (b) It is helpful to have multiple UTxOs in the Treasury
  let _contract_utxos: UTxO[] = [];

  // useQuery hook from Apollo Client does the heavy lifting
  const { data, loading, error } = useQuery(TREASURY_QUERY, {
    variables: {
      contractAddress: treasury.address,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  if (data) {
    data.utxos.map((utxoFromQuery: GraphQLUTxO) => {
      const assets: Asset[] = [
        {
          unit: "lovelace",
          quantity: utxoFromQuery.value,
        },
        {
          unit:
            utxoFromQuery.tokens[0].asset.policyId +
            utxoFromQuery.tokens[0].asset.assetName,
          quantity: utxoFromQuery.tokens[0].quantity,
        },
      ];

      // console.log("Treasury Datum:", utxoFromQuery.datum.bytes);

      _contract_utxos.push({
        input: {
          outputIndex: utxoFromQuery.index,
          txHash: utxoFromQuery.txHash,
        },
        output: {
          address: treasury.address,
          amount: assets,
          plutusData: utxoFromQuery.datum?.bytes, // plutusData is our Inline Datum
        },
      });
    });
  }
  // -------- End useQuery --------

  
  
  // When a Contributor presses the Button to Commit to a Project
  const handleProjectCommitment = async () => {
    if (connected) {
      const network = await wallet.getNetworkId()
      if (network == 1) {
        toast({
          title: "Error",
          description: "Please connect to Cardano Preprod Testnet",
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        // alert("Please connect to Cardano Preprod Testnet");
      } else if (!connectedContributorToken) {
        toast({
          title: "Error",
          description: "Missing Contributor Token",
          status: "error",
          duration: 9000,
          isClosable: true
        });
        // alert("Missing Contributor Token")
      } else {
        onConfirmationOpen()
      }
    } else {
      toast({
        title: "Error",
        description: "Please connect a wallet",
        status: "error",
        duration: 9000,
        isClosable: true,
      });
      // alert("Please connect a wallet")
    }
  };

  // When a Contributor presses the Button to Confirm Commitment inside Modal
  const handleProjectCommitmentConfirmation = async () => {
    setTxLoading(true)
    onConfirmationClose()
    try {
      const tx = new Transaction({ initiator: wallet })
        .redeemValue({
          value: _contract_utxos[0],
          script: treasuryReferenceUTxO,
          datum: _contract_utxos[0],
          redeemer: currentTreasuryRedeemer,
        })
        .sendValue(
          {
            address: treasury.address,
            datum: {
              value: treasuryDatum,
              inline: true,
            },
          },
          utxoBackToTreasury
        )
        .sendValue(
          {
            address: escrow.address,
            datum: {
              value: constructedProjectDatum,
              inline: true,
            },
          },
          utxoToProjectEscrow
        )
        .setMetadata(metadataKeyInt, projectTxMetadata);
      console.log("So far so good.", tx);
    
      const unsignedTx = await tx.build();
      const signedTx = await wallet.signTx(unsignedTx, true);
      const txHash = await wallet.submitTx(signedTx);
      setSuccessfulTxHash(txHash);
      toast({
        title: "Success",
        description: "Succesful commit",
        status: "success",
        duration: 9000,
        isClosable: true,
      });
      // onSuccessOpen();
    } catch (error: any) {
      if (error.info) {
        toast({
          title: "Error",
          description: error.info,
          status: "error",
          duration: 9000,
          isClosable: true,
        });
        // alert(error.info);
      } else {
        console.log(error);
      }
    }
    setTxLoading(false);
  }

  const toggleShowDevStuff = () => {
    setShowDevStuff(!showDevStuff);
  };

  let image = {
    imageUrl: "https://cdn-icons-png.flaticon.com/512/4350/4350950.png",
    imageAlt: "Commit to project icon"
  }
 
  // The CommitToProject component that we see on Front End
  return (
    <>
      <Box border="1px" borderWidth='1px' borderRadius="lg" overflow='hidden'>
        {/* --- START GPTE Project 0002 --- */}
        <Box borderRadius='lg' padding='25px'>
        <Image src={image.imageUrl} alt={image.imageAlt}  objectFit='cover'
    maxW={{ base: '100%', sm: '200px' }} margin='0 auto' padding='15px' />
          <Center>
            <HStack>
          <Heading size="lg">Ready to commit?</Heading>
          {
            projectData.status == "Open" &&   
            <Button onClick={handleProjectCommitment} colorScheme="green" my="2">
              {txLoading ? (
                <Center>
                  <Spinner speed="1.0s"/>
                </Center>
              ) : (
                <Text>Commit</Text>
              )}
            </Button>
          }
            </HStack>
          </Center>
          {/* <Box p="2" bg="teal.900">
            <Text py="1">Commitment Status</Text>
            {loading ? (
              <Center>
                <Spinner/>
              </Center>
            ) : (
              <Text>{successfulTxHash}</Text>
            )}
          </Box> */}
          {/* <Button onClick={toggleShowDevStuff} colorScheme="purple" my="4">
            View Dev Details
          </Button> */}
        </Box>
        {/* --- END GPTE Project 0002 --- */}

        {/* --- For Learning Purposes: --- */}
        {showDevStuff && (
          <Box p="5">
            <Heading size="lg" py="3">
              If you are learning Cardano development, this is for you:
            </Heading>
            <Accordion allowToggle>
              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Project Datum:
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb="4" fontSize="xs">
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(currentProjectDatum, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Project Datum, Constructed for Tx:
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb="4" fontSize="xs">
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(constructedProjectDatum, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Project Datum Hash (you will see the same hash on
                    cardano-cli):
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb="4" fontSize="xs">
                  <Text p="2">{projectDatumHash}</Text>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Commitment Tx Metadata:
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb="4" fontSize="xs">
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(projectTxMetadata, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Treasury Datum
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel pb="4" fontSize="xs">
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(treasuryDatum, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    TreasuryDatumHash:
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <Text p="2">{treasuryDatumHash}</Text>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Treasury Redeemer
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(currentTreasuryRedeemer, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Utxos at Treasury
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(_contract_utxos, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Connected Contributor Token (if connected wallet holds one)
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(connectedContributorToken, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Assets to Treasury (if there is a Contributor token)
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(utxoBackToTreasury, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>

              <AccordionItem>
                <AccordionButton>
                  <Heading size="md" py="2">
                    Assets to Project (if there is a Contributor token)
                  </Heading>
                  <AccordionIcon />
                </AccordionButton>
                <AccordionPanel>
                  <pre>
                    <code className="language-js">
                      {JSON.stringify(utxoToProjectEscrow, null, 2)}
                    </code>
                  </pre>
                </AccordionPanel>
              </AccordionItem>
            </Accordion>
          </Box>
        )}
      </Box>
      {/* --- Modals --- */}
      <Modal 
        key="commitmentConfermationModal"
        size="xl"
        blockScrollOnMount={false}
        isOpen={isConfirmationOpen}
        onClose={onConfirmationClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="3xl">
            Are you ready to commit?
          </ModalHeader>
          <ModalBody>
            <Text mb="2">You are committing to the following project:</Text>
            <Box mx="5" my="2" p="3" fontSize="sm" borderWidth="1px" borderColor="white" borderRadius="lg">
              <Text><b>ID</b>: {projectData.id}</Text>
              <Text><b>Title</b>: {projectData.title}</Text>
              <Text><b>Ada</b>: {projectData.lovelace / 1000000}</Text>
              <Text><b>Gimbals</b>: {projectData.gimbals}</Text>
              <Text><b>Deadline</b>: {expirationDate}</Text>
              <Text><b>Approval</b>: {approvalProcess[projectData.approvalProcess - 1].name}</Text>
            </Box>
            <Text mt="5">Rewards will be distributed to:</Text>
            <Box m="1" pl="2" fontSize="sm">
              <Flex py="1">
                <Text>- <b>Address</b>:&nbsp;</Text>
                <Text _hover={{ cursor: "pointer" }} onClick={() => {
                  setValue(contribAddress)
                  setCopyValue(true)
                  toast({
                    title: "Copied address",
                    status: "success",
                    duration: 2500,
                    isClosable: true,
                  })
                }}>
                  {contribAddress.slice(0,28).concat("...").concat(contribAddress.slice(80, 108))}
                </Text>
              </Flex>
              <Text py="1">- <b>PKH</b>: {connectedPkh}</Text>                  
              <Text py="1" mb="3">- <b>Contributor Token</b>: {connectedContributorToken?.assetName}</Text>
            </Box>
            <Alert status='warning' color="gray.700 "fontSize="sm" borderRadius="md">
              <AlertIcon />
              Once locked, only issuers will be able to unlock the tokens by distributing the 
              reward or canceling the commitment after the deadline 
            </Alert>
          </ModalBody>
          <ModalFooter>
            <Button variant="ghost" mr="3" onClick={onConfirmationClose} _hover={{ bg:"white", color:"gray.700" }}>Cancel</Button>
            <Button bg="green.500" onClick={handleProjectCommitmentConfirmation} _hover={{ bg:"green.600" }}>Commit</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
      <Modal 
        key="successCommitmentModal" 
        size="lg" 
        blockScrollOnMount={false} 
        isOpen={isSuccessOpen} 
        onClose={onSuccessClose}
      >
        <ModalOverlay />
        <ModalContent>
          <ModalHeader fontSize="3xl">
            Successful Commitment Transaction!
          </ModalHeader>
          <ModalBody>
            <Text py="2">Transaction ID: {successfulTxHash}</Text>
            <Text py="2">It may take a few minutes for this tx to show up on a blockchain explorer.</Text>
          </ModalBody>
          <ModalFooter>
            <Button bg="white" color="gray.700" onClick={onSuccessClose}>Close</Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </>
  );
};

export default CommitToProject;
