---
id: "0006"
datePosted: "2022-09-29"
title: "Build a Commitment Tx Error Modal"
lovelace: 50000000
gimbals: 1000
status: "Open"
devCategory: "Front End"
bbk: ["0004", "0005"]
approvalProcess: 2
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
Build a [Modal](https://chakra-ui.com/docs/components/modal/usage) that appears when there is an Error in a Commitment Transaction.

## Requirements:
- When there is an Error in Tx Submission, a modal appears with a relevant message to Contributor.
- Try to cover basic cases like Wallet not connected, Collateral not set, or No Contributor Token present.
- As a challenge, add more exotic errors!

## How To Start:
- Review Projects 0004 and 005.
- Read the [Chakra UI Modal documentation](https://chakra-ui.com/docs/components/modal/usage). Be sure to click the buttons in the Usage examples.
- Run [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) locally and navigate to [localhost:3000/Projects/0001](http://localhost:3000/Projects/0001) to see the context for this task.
- Choose an existing `commitToProject___.tsx` component to work on. For example if you made one for Project 0002, 0004 or 0005, you can continue to build on it here.

## How To Complete:
- Note that just like Projects 0001 and 0002, there could be multiple styles for this modal. To submit your results for this one, please use a unique `commitToProject___.tsx` file.
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) with your new `commitToProject` component.
