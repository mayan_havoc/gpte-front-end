import {
  Box,
  Heading,
  Text,
  Grid,
  Center,
  Spinner,
  Link as ChakraLink,
} from "@chakra-ui/react";
import { getSortedProjectsData } from "../../project-lib/projects";
import type { Project } from "../../types";
// import ProjectCardRounded from "../../components/project/ProjectCardUli";
import ProjectCard from "../../components/project/ProjectCardWithImage";

import { useQuery, gql } from "@apollo/client";
import { escrow } from "../../cardano/plutus/escrowContract";
import Link from "next/link";

const TO_ESCROW_QUERY = gql`
  query TransactionsToEscrow($escrowAddress: String!) {
    transactions(where: { outputs: { address: { _eq: $escrowAddress } } }) {
      metadata {
        value
      }
      outputs {
        address
        tokens {
          asset {
            assetName
            policyId
          }
        }
      }
    }
  }
`;

const FROM_ESCROW_QUERY = gql`
  query TransactionsFromEscrow($escrowAddress: String!) {
    transactions(where: { inputs: { address: { _eq: $escrowAddress } } }) {
      metadata {
        value
      }
      inputs {
        address
        tokens {
          asset {
            assetName
            policyId
          }
        }
      }
    }
  }
`;

export async function getStaticProps() {
  const allProjectsData = getSortedProjectsData();
  return {
    props: {
      allProjectsData,
    },
  };
}

type Props = {
  allProjectsData: Project[];
};

const Projects: React.FC<Props> = ({ allProjectsData }) => {
  const toEscrowQuery = useQuery(TO_ESCROW_QUERY, {
    variables: {
      escrowAddress: escrow.address,
    },
  });

  const fromEscrowQuery = useQuery(FROM_ESCROW_QUERY, {
    variables: {
      escrowAddress: escrow.address,
    },
  });
  const error = toEscrowQuery.error || fromEscrowQuery.error;
  const loading = toEscrowQuery.loading || fromEscrowQuery.loading;

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  const frontEndProjects = allProjectsData.filter(
    (project) => project.devCategory === "Front End"
  );
  const plutusProjects = allProjectsData.filter(
    (project) => project.devCategory === "Plutus"
  );
  const dataProjects = allProjectsData.filter(
    (project) => project.devCategory === "Data"
  );
  const documentationProjects = allProjectsData.filter(
    (project) =>
      project.devCategory === "Documentation" ||
      project.devCategory === "Education"
  );
  const contribTokenProjects = allProjectsData.filter(
    (project) => project.devCategory === "Contributor Token"
  );
  const verificationProjects = allProjectsData.filter(
    (project) => project.devCategory === "Verification"
  );

  return (
    <Box w="90%" mx="auto">
      <Heading py="5" size="4xl">
        Gimbal Project Treasury and Escrow
      </Heading>
      <Text p="2" fontSize="xl">
        This initial set of Projects and Tasks provides a hands-on way for
        Contributors to get to know the GPTE system while contributing to its
        development. We will know that our work is successful if other teams are
        able to use, fork, remix or otherwise adapt this Dapp to get stuff done,
        track contributions, and build the reputation of a distributed community
        of Cardano developers.
      </Text>
      <Heading p="2" size="lg">
        Goal #1:
      </Heading>
      <Text p="2" fontSize="xl">
        Make GPTE robust, reliable, and accesible so that other teams can use
        it.
      </Text>
      <Heading p="2" size="lg">
        Goal #2:
      </Heading>
      <Text p="2" fontSize="xl">
        Build the capacity of the Gimbalabs developer community to help other
        teams implement this Dapp.
      </Text>
      <Text p="2" fontSize="xl">
        <Link href="/projects/approved-projects">
          <ChakraLink>View list of Approved Projects</ChakraLink>
        </Link>
      </Text>
      <Box my="5" border="1px" />
      <Heading py="5" size="2xl">
        Front End Projects
      </Heading>
      <Grid templateColumns="repeat(auto-fill, minmax(300px,1fr))" gap="10">
        {frontEndProjects.map((p: Project) => (
          <ProjectCard
            key={p.id}
            project={p}
            txToEscrow={toEscrowQuery.data.transactions}
            txFromEscrow={fromEscrowQuery.data.transactions}
          />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Plutus Projects
      </Heading>
      <Grid templateColumns="repeat(auto-fill, minmax(300px,1fr))" gap="5">
        {plutusProjects.map((p: Project) => (
          <ProjectCard
            key={p.id}
            project={p}
            txToEscrow={toEscrowQuery.data.transactions}
            txFromEscrow={fromEscrowQuery.data.transactions}
          />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Verification Projects
      </Heading>
      <Grid templateColumns="repeat(auto-fill, minmax(300px,1fr))" gap="5">
        {verificationProjects.map((p: Project) => (
          <ProjectCard
            key={p.id}
            project={p}
            txToEscrow={toEscrowQuery.data.transactions}
            txFromEscrow={fromEscrowQuery.data.transactions}
          />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Documentation and Education Projects
      </Heading>
      <Grid templateColumns="repeat(auto-fill, minmax(300px,1fr))" gap="5">
        {documentationProjects.map((p: Project) => (
          <ProjectCard
            key={p.id}
            project={p}
            txToEscrow={toEscrowQuery.data.transactions}
            txFromEscrow={fromEscrowQuery.data.transactions}
          />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Contributor Token Projects
      </Heading>
      <Grid templateColumns="repeat(auto-fill, minmax(300px,1fr))" gap="5">
        {contribTokenProjects.map((p: Project) => (
          <ProjectCard
            key={p.id}
            project={p}
            txToEscrow={toEscrowQuery.data.transactions}
            txFromEscrow={fromEscrowQuery.data.transactions}
          />
        ))}
      </Grid>
      <Box my="5" border="1px" />
      <Heading py="10" size="2xl">
        Data and Tracking Projects
      </Heading>
      <Grid templateColumns="repeat(auto-fill, minmax(300px,1fr))" gap="5">
        {dataProjects.map((p: Project) => (
          <ProjectCard
            key={p.id}
            project={p}
            txToEscrow={toEscrowQuery.data.transactions}
            txFromEscrow={fromEscrowQuery.data.transactions}
          />
        ))}
      </Grid>
    </Box>
  );
};

export default Projects;
