import { Box, Heading, Text, Link, Grid, GridItem, Center, Spinner, Flex, List, ListItem, UnorderedList } from "@chakra-ui/react";
import { getAllProjectIds, getProjectData } from "../../project-lib/projects";
import { Project } from "../../types";
import CommitToProject from "../../components/transactions/commitToProject_robertom";
import ContributorTokens from "../../components/contributor/ContributorTokens";

import styles from "../../styles/ProjectPage.module.css";
import { approvalProcess } from "../../project-lib/approvalProcess";

import { GetStaticProps, GetStaticPaths } from "next";
import { ParsedUrlQuery } from "querystring";
import CommitmentNote from "../../components/about-instance/CommitmentNote";

import { useQuery, gql } from "@apollo/client";
import { escrow } from "../../cardano/plutus/escrowContract";
import { filterTransactionsById, getContributorTokenNames, getCommitmentStatus } from "../../components/tracking/CommitmentStatus";

import ProjectDetails from "../../components/project/ProjectDetails";

const TO_ESCROW_QUERY = gql`
  query TransactionsToEscrow($escrowAddress: String!) {

    transactions(where: { outputs: { address: { _eq: $escrowAddress } } }) {
      metadata {
        value
      }
      outputs {
        address
        tokens {
          asset {
            assetName
            policyId
          }
        }
      }
    }
  }
`;

const FROM_ESCROW_QUERY = gql`
  query TransactionsFromEscrow($escrowAddress: String!) {

    transactions(where: { inputs: { address: { _eq: $escrowAddress } } }) {
      metadata {
        value
      }
      inputs {
        address
        tokens {
          asset {
            assetName
            policyId
          }
        }
      }
    }
  }
`;

interface IParams extends ParsedUrlQuery {
  id: string;
}

export const getStaticProps: GetStaticProps = async (context) => {
  const { id } = context.params as IParams;

  const projectData = await getProjectData(id);
  return {
    props: {
      projectData,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  let paths: { params: { id: string } }[] = [];

  if (getAllProjectIds) {
    paths = getAllProjectIds();
  }
  return {
    paths,
    fallback: false,
  };
};

type Props = {
  projectData: Project;
};

const ProjectPage: React.FC<Props> = ({ projectData }) => {

  const toEscrowQuery = useQuery(TO_ESCROW_QUERY, {
    variables: {
      escrowAddress: escrow.address,
    },
  });

  const fromEscrowQuery = useQuery(FROM_ESCROW_QUERY, {
    variables: {
      escrowAddress: escrow.address,
    },
  });
  const error = toEscrowQuery.error || fromEscrowQuery.error;
  const loading = toEscrowQuery.loading || fromEscrowQuery.loading;


  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }


  const txToEscrow = filterTransactionsById(toEscrowQuery.data.transactions, projectData.id);
  const txFromEscrow = filterTransactionsById(fromEscrowQuery.data.transactions, projectData.id);

  let status = getCommitmentStatus(getContributorTokenNames(txToEscrow, "to"),getContributorTokenNames(txFromEscrow, "from"), projectData.multipleCommitments)

  return (
    <Grid templateColumns="repeat(10, 1fr)" gap="5">
      <GridItem colSpan={6} rowSpan={5}>
        <Box bg="gray.700" p="5" fontSize="lg">
          <Heading py="3" size="xl">
            {projectData.title}
          </Heading>
          <Box py="3">
            <div
              dangerouslySetInnerHTML={{ __html: projectData.contentHtml }}
              className={styles.mdStyle}
            />
            <Box border="1px" my="5" />
            <Heading py="3">
              Approval Process:{" "}
              {approvalProcess[projectData.approvalProcess - 1].name}
            </Heading>
            <Text>
              {approvalProcess[projectData.approvalProcess - 1].description}
            </Text>
          </Box>
        </Box>
      </GridItem>
      <GridItem colSpan={4}>
        <Box mb="5" p="5" border="1px" borderRadius="lg">
          <Heading>Commitment Status</Heading>
          <Flex>
            <Text mt="5" mb="5" as='b' fontSize='lg'>Status : &nbsp;</Text>
            {
            status[0] == "Open" ?
            <Text mt="5" mb="5" as='b' fontSize='lg' color='Green'>{status[0]}</Text>
            :
            <Text mt="5" mb="5" as='b' fontSize='lg' color='Red'>{status[0]}</Text>
            }
          </Flex>
          <Box mb="5" p="5" border="1px" borderRadius="md">
            <Text as='b' fontSize='lg'>Live Commitments :<br/><br/></Text>
            {
              status[1].length == 0 ?
                <Text as='b' fontSize='lg'>None</Text>
              :
                <UnorderedList>{
                  status[1].map((tokenName:string, i) => (
                    <ListItem key={i}><Text as='b' fontSize='lg' color='Green'>{tokenName}<br/></Text></ListItem>
                    ))
                  }</UnorderedList>
            }
          </Box>
          <Flex>
            <Text as='b'>Multiple Commitment : &nbsp;</Text>{projectData.multipleCommitments == true? <Text> Allowed</Text>:<Text> Not Allowed</Text>}
          </Flex>
        </Box>
      </GridItem>
      <GridItem colSpan={4}>
        <CommitmentNote />
      </GridItem>
      <GridItem colSpan={4}>
        {/* A separate Component: */}
        <ContributorTokens />
      </GridItem>
      <GridItem colSpan={4}>
        {/* React allows us to extract this Box to a separate Component */}
        {/* Give it a try on Project 0018 */}
        <ProjectDetails projectData={projectData}/>
      </GridItem>
      {/* Learn about Chakra UI Grid System: */}
      {/* If you are working on Project 0002, try this: */}
      {/* 1. First, remove the "For Learning" section of <CommitToProject /> */}
      {/* 2. Then, change colSpan in GridItem to move your smaller CommitToProject */}
      {/*    up to the top right side of the page, under "Project Details" GridItem. */}
      {/* Not sure how? Change the colSpan numbers and look at changes to get a feel for how it works. */}
      {
      status[0] == "Open" ?
        <GridItem colSpan={4}>
          <CommitToProject projectData={projectData} />
        </GridItem>
      :
        null
      }
    </Grid>
  );
};

export default ProjectPage;
