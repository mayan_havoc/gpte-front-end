---
id: "0010"
datePosted: "2022-09-29"
title: "Create a Video Guide for how to set up a GPTE Instance"
lovelace: 200000000
gimbals: 4000
status: "Coming Soon"
devCategory: "Education"
bbk: [""]
approvalProcess: 3
multipleCommitments: false
repositoryLink: ""
---

## Outcome:
- Record video documentation of the steps required to set up a GPTE Instance.

## Requirements:
- Deliver a video showing all high-level steps.


## How To Start:

## Links + Tips:
-

## How To Complete:
