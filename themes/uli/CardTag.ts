import {chakra,Tag} from "@chakra-ui/react";

const CardTag = chakra(Tag, {
    baseStyle: {
      justifyContent: "center",
      fontWeight: "bold",
      mt: "2",
      color: "whiteAlpha.900",
      borderRadius: "full",
    },
  });
export default CardTag
   